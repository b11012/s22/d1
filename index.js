// Array Methods

//Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

// Mutator Methods
/*
    - Mutator methods are functions that "mutate" or change an array after they're created
    - These methods manipulate the original array performing various tasks such as adding and removing elements
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log(fruits);

// push()
/*
	- Adds an element in the end of an array AND returns the array's length
	Syntax:
		arrayName.push("value");
*/
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
// result: 5
console.log("Mutated through push: " + fruits);
console.log(fruits);

// add multiple elements in an array
fruits.push("Avocado", "Guava");
console.log("Mutated with multiple elements: " + fruits);
console.log(fruits);

// pop();
/*
	- Removes the last element in an array AND returns the removed element
	Syntax: 
		arrayName.pop();
*/
let removedFruit =  fruits.pop();
console.log(removedFruit);
// result: Guava
console.log("Mutated through pop: " + fruits);
console.log(fruits);

// unshift();
/*
	- Adds one or more elements at the beginning of an array
	Syntax: arrayName.unshift("value");
			arrayName.unshift("valueA", "valueB");
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated through unshift: " + fruits);
console.log(fruits);

// shift()
/*
	- Removes an element at the beginning of an array AND returns the removed element
	Syntax:
		arrayName.shift();
*/
let anotherFruit = fruits.shift();
console.log(anotherFruit);
// result: Lime
console.log("Mutated through shift: " + fruits);
console.log(fruits);

// function removeFirst(){
// 	fruits.shift()
// 	console.log(fruits);
// }
// removeFirst();

// splice()
/*
	- Simultaneously removes elements from a specified index number and adds elements
	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

		startingIndex- The index at which to start changing the array.
			note: If greater than the length of the array, start will be set to the length of the array. In this case, no element will be deleted but the method will behave as an adding function, adding as many elements as items provided.
		
		deleteCount- An integer indicating the number of elements in the array to remove from start.
			note: If deleteCount is omitted, or if its value is equal to or larger than array.length then all the elements from start to the end of the array will be deleted.

		elementsToBeAdded- The elements to add to the array, beginning from start.
*/

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated through splice: " + fruits)
console.log(fruits);

// another example:
fruits.splice(3, 1, "Orange");
console.log("Mutated through splice again: " + fruits);
console.log(fruits);

// another example:
fruits.splice(0, 2);
console.log("Mutated through splice without adding: " + fruits)
console.log(fruits);

// fruits.splice(0, fruits.length);
// console.log("here " + fruits);

// let fruits2 = fruits.splice(0, fruits.length-1);
// console.log(fruits2);

// sort();
/*
	- Rearranges the array elements in alphanumeric order
	Syntax:
		arrayName.sort();
*/
fruits.sort();
console.log("Mutated through sort: " + fruits);
console.log(fruits);

// reverse()
/*	
	- Reverses the order of array elements
	Syntax:
		arrayName.reverse();
*/
fruits.reverse();
console.log("Mutated through reverse: " + fruits);
console.log(fruits);

// Non-Mutator Methods
/*
    - Non-Mutator methods are functions that do not modify or change an array after they're created
    - These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
*/
console.log("Non-Mutator Methods starts here:")
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];


// indexOf()
/*
	- Returns the index number of the first matching element found in an array
	- If no match was found, the result will be -1.
	- The search process will be done from first element proceeding to the last element
	Syntax: 
		arrayName.indexOf(searchValue)
*/

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf: " + firstIndex);
// result: 1

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf again: " + invalidCountry);
// result: -1

// lastIndexOf()
/*
	
	- Returns the index number of the last matching element found in an array
	Syntax:
		arrayName.lastIndexOf(searchValue)
		arrayName.lastIndexOf(searchValue, fromIndex)
*/

let lastIndex = countries.lastIndexOf("PH");
console.log('Result of lastIndexOf ' + lastIndex);
// result: 5

let countries2 = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE', 'PH'];
let lastIndexStart = countries2.lastIndexOf("PH", 7)
console.log(countries2[7]);
console.log('Result of lastIndexOf again ' + lastIndexStart);
// result: 5 (notes: for updates)
// console.log(countries);

// slice()
/*
	Syntax: 
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/
console.log(countries);
let slicedArrayA = countries.slice(2);
console.log('Result from slice with startingIndex only: ' + slicedArrayA);
// result: 
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice with startingIndex and endingIndex: ' + slicedArrayB);
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Result from slice with negative value: ' + slicedArrayC)
console.log(slicedArrayC);
console.log(countries);

// toString()
/*
	Syntax:
		arrayName.toString();
*/
let stringArray = countries.toString();
console.log('Result from toString: ');
console.log(stringArray);

// concat()
/*
	Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/
let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe bootstrap'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method: ')
console.log(tasks);

// combining multiple arrays
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log('Result from concat with all arrays: ');
console.log(allTasks);

// combining arrays with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat with elements: ')
console.log(combinedTasks);

// join()
/*
	Syntax:
		arrayName.join('separator string')
*/
let users = ["John", "Robert", "Jane", "Joe"];

console.log(users.join());
// separator: comma (,)
console.log(users.join(''));
// separator: no spaces
console.log(users.join(' - '));
// result: John - Robert - Jane - Joe

// Iteration Methods

// forEach()
/*
	Syntax: 
		arrayName.forEach(function(indivElement){
			statement / code block
		});
*/

allTasks.forEach(function(task){
	console.log(task);
});

// use forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task){

	console.log(task);

	if(task.length > 10){

		console.log(task);
		filteredTasks.push(task);
	};
});
console.log('Results of filteredTasks: ');
console.log(filteredTasks);

// map();
/*
	Syntax:
		let / const resultArray = arrayName.map(function(indivElement){
			return statement/ code block
		})
*/
 



let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){

	return number * number;
});

console.log('Original array:')
console.log(numbers); // original is unaffected by map()
console.log('Result of map method:')
console.log(numberMap); // a new array returned by map() and stored in a variable

// forEach() vs. map()

let numberForEach = numbers.forEach(function(number){

	return number * number;
});

console.log(numberForEach);
// result: undefined

// every()
/*
	Syntax: 
		let / const resultArray = arrayName.every(function(indivElement){
			return code block / statement (condition)
		});
*/

let allValid = numbers.every(function(number){
	return (number < 3)
});

console.log('Result from every method:')
console.log(allValid);
// result: false

// some()
/*
	- Checks if at least one element in the array meets the given condition
	- Returns a true value if at least one element meets the condition and false if otherwise
	Syntax: 
		let / const resultArray = arrayName.some(function(){
			return statement / code block (condition)
		})

*/

let someValid = numbers.some(function(number){
	return (number < 2);
});

console.log('Result from some method:')
console.log(someValid);
// result: true


// Combining the returned result from the every/some method may be used in other statements to perform consecutive results
if(someValid){
	console.log('Some numbers in the array are less than 2.')
};

// filter()
/*
	- Returns new array that contains elements which meets the given condition
	- Returns an empty array if no elements were found
	- Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods
	- Mastery of loops can help us work effectively by reducing the amount of code we use
	- Several array iteration methods may be used to perform the same result
	Syntax: 
		let / const resultArray = numbers.filter(function(indivElement){
			return statement code block (condition)
		});
*/
let filterValid = numbers.filter(function(number){

	return (number < 3);
});
console.log('Result of filter method:');
console.log(filterValid);

let nothingFound = numbers.filter(function(number){

	 return (number > 5);
});
console.log('Result of filter method with nothing found: ')
console.log(nothingFound);

// function filtering(){
// 	let string = 'supercalifragilisticexpialidocious';

// 	let stringResult = string.filter(function(i){
// 	return
// 	(
// 			string[i].toLowerCase() == 'a' ||
// 			string[i].toLowerCase() == 'e' ||
// 			string[i].toLowerCase() == 'i' ||
// 			string[i].toLowerCase() == 'o' ||
// 			string[i].toLowerCase() == 'u')
		
// 	})

// }
// let stringresults = filtering();
// console.log(stringresults);


// filtering using forEach()

let filteredNumbers = [];

numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number);
	}
})
console.log('Filtering through forEach:')
console.log(filteredNumbers);

// reduce()
/*
	Syntax:
		let / const resultArray = arrayName.reduce(function(accumulator, currentValue){
			return expression / operation
		})

*/

/* 
    - Evaluates elements from left to right and returns/reduces the array into a single value
    - Syntax
        let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
            return expression/operation
        })
    - The "accumulator" parameter in the function stores the result for every iteration of the loop
    - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
    - How the "reduce" method works
        1. The first/result element in the array is stored in the "accumulator" parameter
        2. The second/next element in the array is stored in the "currentValue" parameter
        3. An operation is performed on the two elements
        4. The loop repeats step 1-3 until all elements have been worked on
*/


let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
		console.warn('current iteration: ' + ++iteration);
		console.log('accumulator: ' + x);
		console.log('currentValue: ' + y);

		// operation
		return x + y;
});
console.log('Result from reducedArray: ' + reducedArray);


// includes()
/*
	- includes() method checks if the argument passed can be found in the array.
	- it returns a boolean which can be saved in a variable.
	    - returns true if the argument is found in the array.
	    - returns false if it is not.
	Syntax: 
		arrayName.includes(<argumentToFind>)
*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes('Mouse');
console.log(productFound1);
// result: true

let productFound2 = products.includes("Headset");
console.log(productFound2);
// result: false


/*
    - Methods can be "chained" using them one after another
    - The result of the first method is used on the second method until all "chained" methods have been resolved
    - How chaining resolves in our example:
        1. The "product" element will be converted into all lowercase letters
        2. The resulting lowercased string is used in the "includes" method
*/
let filteredProducts = products.filter(function(product){
		let includesVar = 'laptop'
		return product.toLowerCase().includes(includesVar);
})
console.log(filteredProducts);


// Template Literals and Placeholders

alert(`You have ${friendslist.length} friends. Add one first`);
// back ticks = `${placeholder}`



alert("You have " + friendsList.length + " " + "friends. Add one first");